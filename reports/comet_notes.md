**RWTH Aachen - Deep Learning in Physics Research SS2020**

**Exercise 9 GANs**

Date: 29.06.2020

Students:
| student            | rwth ID |
|--------------------|---------|
| Baptiste Corneglio | 411835|
| Florian Stadtmann  | 367319 |
| Johannes Wasmer    |  090800 |

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Preprocessing the input data](#preprocessing-the-input-data)
- [Analyzing the provided models](#analyzing-the-provided-models)
    - [Discriminator](#discriminator)
    - [Generator](#generator)
    - [GAN](#gan)
- [Task 1: Generating MNIST digits using a Generative Adversarial Networks [17 P]](#task-1-generating-mnist-digits-using-a-generative-adversarial-networks-17-p)
    - [task1.I. Pretrain the discriminator and evaluate the test accuracy [2 P]](#task1i-pretrain-the-discriminator-and-evaluate-the-test-accuracy-2-p)
        - [Discussion](#discussion)
        - [Code](#code)
        - [Output](#output)
    - [task1.II. Set up the main loop for training the adversarial framework (see slide 29) [10 P]](#task1ii-set-up-the-main-loop-for-training-the-adversarial-framework-see-slide-29-10-p)
        - [Discussion](#discussion-1)
        - [Code](#code-1)
    - [task1.III. Generate some images after each training epoch. Plot training losses and generated images [5 P]](#task1iii-generate-some-images-after-each-training-epoch-plot-training-losses-and-generated-images-5-p)
        - [Introduction](#introduction)
        - [Code](#code-2)
        - [Results & Discussion](#results--discussion)
            - [Plots for Experiment 0](#plots-for-experiment-0)
            - [Plots for Experiment 1](#plots-for-experiment-1)
            - [Plots for Experiment 2](#plots-for-experiment-2)
            - [Plots for Experiment 3](#plots-for-experiment-3)
- [References](#references)

<!-- markdown-toc end -->

# Preprocessing the input data #

The input data is the usual MNIST dataset. Different normalization/preprocessing are possible.

``` text
X_train stats before preprocessing:
shape (60000, 28, 28), type uint8
(min,max)=(0.00e+00,2.55e+02), mean=3.33e+01, std=7.86e+01

X_train stats after preprocessing, style Exercise9.py as provided:
shape (60000, 28, 28, 1), type float64
(min,max)=(0.00e+00,1.00e+00), mean=1.31e-01, std=3.08e-01

X_train stats after preprocessing, style standardize(X_train):
shape (60000, 28, 28), type float32
(min,max)=(-1.27e+00,2.45e+02), mean=5.48e-10, std=9.56e-01

X_train stats after preprocessing, style tensorflow GAN MNIST tutorial [3]:
shape (60000, 28, 28, 1), type float32
(min,max)=(-1.00e+00,1.00e+00), mean=-7.39e-01, std=6.16e-01
```

As we noted before in our submission exercise4: 

Stanford class CS231n Convolutional neural networks recommends [5]:

> The recommended preprocessing is to center the data to have mean of zero, and normalize its scale to [-1, 1] along each feature.

The standardization has to be performed relative to the training data [5]:

> An important point to make about the preprocessing is that any preprocessing statistics (e.g. the data mean) must only be computed on the training data, and then applied to the validation / test data.

On the other hand, it makes sampling from latent space (uniform distribution on the interval `[0,1)`) more convenient if we can sample from the interval [0,1], and the mean and standard deviation deviate not so much from (0,1), so we choose to go with the provided `Exercise9.py` preprocessing. 

# Analyzing the provided models #

Here we compare the GAN architecture implementations from the exercise assignment without modifications (`gan.py`), from machinelearningmastery GAN MNIST tutorial [2], and from tensorflow GAN MNIST tutorial [3].  

## Discriminator ##

`gan.py` uses as convolutional pyramid (CP) 1 conv block (CB), consisting of two Conv2D layer with 256 and 512 filters respectively, kernel size 5x5, same padding, stride 2x2, `relu` activation. After flattening, the fully connected network (FCN, FCB for 'block') consists of one Dense layer with 256 units, and an `l1_l2` activity regularizer with factor 1e-5. The output layer is Dense with 2 units for classification of real and dreamed (fake) images, with `softmax` activation. All conv and Dense layers except the output layer have downstream a LeakyReLU layer with alpha=0.2, and a Dropout layer with def rate 0.25.

The GAN for MNIST from machinelearningmastery [2] uses the same architecture with slightly different parameters and only one Dense output layer in the FCN.

The DCGAN for MNIST from the tensorflow tutorials [3] uses nearly the same architecture as well.

|                 | `gan.py`              | [2]                   | [3]                  |
| :--             | :--                   | :--                   | :--                  |
| CBs:            | 2                     | 2                     | 2                    |
| filters         | 256, 512              | 64, 64                | 64,128               |
| kernel_size     | 5x5                   | 3x3                   | 5x5                  |
| stride          | 2x2                   | 2x2                   | 2x2                  |
| padding         | same                  | same                  | same                 |
| activation      | `relu`                | `relu`                | `None`               |
| LeakyReLU alpha | 0.2                   | 0.2                   | 0.3                  |
| Dropout rate    | 0.25                  | 0.4                   | 0.3                  |
| :--             | :--                   | :--                   | :--                  |
| FCBs:           | 1                     | 0                     | 0                    |
| units           | 256                   | None                  | None                 |
| `l1_l2`         | 1e-5                  | None                  | None                 |
| output units    | 2                     | 1                     | 1                    |
| output actvtn   | `softmax`             | `binary_crossentropy` | None                 |
| :--             | :--                   | :--                   | :--                  |
| Compilation:    |                       |                       | (None, tensorflow)   |
| loss            | `binary_crossentropy` |                       | `BinaryCrossentropy` |
| optimizer       | ADAM                  | ADAM                  | ADAM                 |
| opt init params | `lr=1e-3`             | `lr=2e-4, beta=0.5`   | `lr=1e-4`            |

From [Layer weight regularizers](https://keras.io/api/layers/regularizers/):
``` text
activity_regularizer: Regularizer to apply a penalty on the layer's output

l1_l2 function
tf.keras.regularizers.l1_l2(l1=0.01, l2=0.01)
Create a regularizer that applies both L1 and L2 penalties.
```

From the `LeakyReLU` docstring:
``` text
Leaky version of a Rectified Linear Unit.

It allows a small gradient when the unit is not active:
  f(x) = alpha * x if x < 0
  f(x) = x if x >= 0

Arguments:
  alpha: Float  = 0. Negative slope coefficient. Default to 0.3.  
```

Model summary of the unmodified `gan.py` discriminator:

``` text
Model: "Generator"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
input_1 (InputLayer)         [(None, 100)]             0         
_________________________________________________________________
dense (Dense)                (None, 39200)             3959200   
_________________________________________________________________
batch_normalization (BatchNo (None, 39200)             156800    
_________________________________________________________________
activation (Activation)      (None, 39200)             0         
_________________________________________________________________
reshape (Reshape)            (None, 14, 14, 200)       0         
_________________________________________________________________
up_sampling2d (UpSampling2D) (None, 28, 28, 200)       0         
_________________________________________________________________
conv2d (Conv2D)              (None, 28, 28, 100)       180100    
_________________________________________________________________
batch_normalization_1 (Batch (None, 28, 28, 100)       400       
_________________________________________________________________
activation_1 (Activation)    (None, 28, 28, 100)       0         
_________________________________________________________________
conv2d_1 (Conv2D)            (None, 28, 28, 50)        45050     
_________________________________________________________________
batch_normalization_2 (Batch (None, 28, 28, 50)        200       
_________________________________________________________________
activation_2 (Activation)    (None, 28, 28, 50)        0         
_________________________________________________________________
conv2d_2 (Conv2D)            (None, 28, 28, 1)         51        
=================================================================
Total params: 4,341,801
Trainable params: 4,263,101
Non-trainable params: 78,700
_________________________________________________________________
```

## Generator ##

As noted in the lecture, in the generator architecture we create a learnable mapping from latent space (uniform noise) by decreasing feature space dimension and increasing spatial extent, i.e. creating an image of a MNIST digit out of a latent space random vector. In the first hidden layer, we are permitted to use *one* `Dense` layer with `h * w * c` units, where `hxw` is the initial (small) image resolution, and `c` the number of channels or feature space dimension, i.e. many versions of the same image for the filters in the transpose convolutional layers downstream to learn discernible features from. Then a `Reshape` layer turns this into an image with spatial extent or shape `h x w x c`. From here on, we 'upsample' the image, either by pairing an `UpSampling2D` and a `Conv2D` layer, or by equivalently using `Conv2DTranspose` layers (TCB = transposed Convolutional blocks), where a stride of 2x2 corresponds to an upsampling of 2x2, ie doubling of `h` and `w` respectively. Using a kernel size the factor of the stride keeps down the forming of checkerboard patterns [2]. Normalizing in-between layers with `BatchNormalization` keeps down the vanishing gradients problem [1]. This is reapeated in reverse CP until we arrive at a 28x28 MNIST-scale image. The output layer is a `Conv2D` layer with one channel for MNIST grayscale.

(Note: the tensor input/output shapes are actually not `(h,w,c)`, but `(None,h,w,c)`, where `None` is replaced by the batch size during training.)

`gan.py` scales from 14x14 28x28 after the first conv layer. [2] scales from 7x7 to 28x28 over two conv layers. Note: in the table, the ouput `Conv2D` layer is the last of the TCBs.

|                      | `gan.py`        | [2]              | [3]                  |
| :--                  | :--             | :--              | :--                  |
| FCBs:                | 1               | 1                | 1                    |
| hxwxc                | 14x14x200       | 7x7x128          | 7x7x256              |
| :--                  | :--             | :--              | :--                  |
| TCBs:                | 2               | 2                | 2                    |
| channels             | `c//2, c//4, 1` | 128, 128, 1      | 128, 64, 1           |
| kernel_size          | 3x3, 3x3, 1x1   | 4x4, 4x4, 7x7    | 5x5, 5x5, 5x5        |
| stride/UpSampling    | 2x2, 1x1, 1x1   | 2x2, 2x2, 1x1    | 1x1, 2x2, 2x2        |
| padding              | same            | same             | same                 |
| activation           | `relu`          | `LeakyReLU(0.2)` | `LeakyReLU(0.3)`     |
| `BatchNormalization` | yes             | no               | yes                  |
| output actvtn        | `sigmoid`       | `sigmoid`        | `tanh`               |
| :--                  | :--             | :--              | :--                  |
| Compilation:         | None            | None             | (None, tensorflow)   |
| loss                 | None            | None             | `BinaryCrossentropy` |
| optimizer            | None            | None             | ADAM                 |
| opt init params      | None            | None             | `lr=1e-4`            |


Model summary of the unmodified `gan.py` discriminator:

``` text
Model: "Discriminator"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
input_2 (InputLayer)         [(None, 28, 28, 1)]       0         
_________________________________________________________________
conv2d_3 (Conv2D)            (None, 14, 14, 256)       6656      
_________________________________________________________________
leaky_re_lu (LeakyReLU)      (None, 14, 14, 256)       0         
_________________________________________________________________
dropout (Dropout)            (None, 14, 14, 256)       0         
_________________________________________________________________
conv2d_4 (Conv2D)            (None, 7, 7, 512)         3277312   
_________________________________________________________________
leaky_re_lu_1 (LeakyReLU)    (None, 7, 7, 512)         0         
_________________________________________________________________
dropout_1 (Dropout)          (None, 7, 7, 512)         0         
_________________________________________________________________
flatten (Flatten)            (None, 25088)             0         
_________________________________________________________________
dense_1 (Dense)              (None, 256)               6422784   
_________________________________________________________________
leaky_re_lu_2 (LeakyReLU)    (None, 256)               0         
_________________________________________________________________
dropout_2 (Dropout)          (None, 256)               0         
_________________________________________________________________
dense_2 (Dense)              (None, 2)                 514       
=================================================================
Total params: 9,707,266
Trainable params: 0
Non-trainable params: 9,707,266
_________________________________________________________________
```

To confirm that the untrained generator just spits out noise from the latent space, combined with the noise from the random weights, we can generate an image. And then see what the untrained generator makes of it, i.e. 

``` python
# confirm that an untrained generator just spits out noise
noise = tf.random.normal([1, 100])
generated_image = generator(noise, training=False)
plot_and_log_image('generator_untrained_output.png', path_data, 
                   generated_image[0, :, :, 0], figsize=(2.5,2.5))
                   
# confirm that the untrained discriminator can't discriminate
decision = discriminator(generated_image).numpy()
printcomet("decision from untrained discriminator on image from untrained generator"\
           "(probabilities of image being dreamed or real: {}".format(decision))                   
```

Output:

![generator_untrained_output.png](https://www.comet.ml/api/image/notes/download?imageId=9nE2muEe5Iqb7DVLGAc1iyOOL&objectId=008b607c5ed743cf8ae7a9606aabdb09)

```text
decision from untrained discriminator on image from untrained generator(probabilities of image being dreamed or real: [[0.49359387 0.5064062 ]]
```

## GAN ##

The GAN (or DCGAN for 'Deep Convolutional Adversarial Network', which here means the same thing) is by building a new model `gan` with the setup `latent space input -> generator -> discriminator`, which can be achieved in a number of ways.

```python
gan_input = Input(shape=[latent_dim])
gan_output = discriminator(generator(gan_input))
gan = Model(gan_input, gan_output)

# equivalent:
gan = Sequential()
gan.add(generator)
gan.add(discriminator)
```

A crucial point here is the correct configuration order. In pseudocode:

```
define discriminator model D
compile D
define generator model G
compile G
define gan model GAN(d,g)
--> make_trainable(d, False) <--
compile GAN
```

The better the discriminator gets at spotting dream images / fakes, the more the
generator must adjust its weights to outsmart the former. But we don't want the
discriminator to overtrain on the dream images while they are passed through the
GAN model. That can be prevented by freezing its weights before the `GAN`
compilation, as shown above. Then the weights of the discriminator in `GAN` are frozen, while the same weights can be trained separately from the GAN by training `D`.  

The tensorflow tutorial [3] does not do this (i.e., compile models), instead it
defines its own tensorflow-based training and loss computation.

|                 | `gan.py`              | [2]                   | [3]                  |
| :--             | :--                   | :--                   | :--                  |
| Compilation:    |                       |                       | (None, tensorflow)   |
| loss            | `binary_crossentropy` |                       | `BinaryCrossentropy` |
| optimizer       | ADAM                  | ADAM                  | ADAM                 |
| opt init params | `lr=1e-4`             | `lr=2e-4, beta=0.5`   | `lr=1e-4`            |


Model summary of the unmodified `gan.py` GAN:

``` text
Model: "GAN"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
input_3 (InputLayer)         [(None, 100)]             0         
_________________________________________________________________
Generator (Model)            (None, 28, 28, 1)         4341801   
_________________________________________________________________
Discriminator (Model)        (None, 2)                 9707266   
=================================================================
Total params: 14,049,067
Trainable params: 4,263,101
Non-trainable params: 9,785,966
_________________________________________________________________
```

# Task 1: Generating MNIST digits using a Generative Adversarial Networks [17 P] #

## task1.I. Pretrain the discriminator and evaluate the test accuracy [2 P] ##

### Discussion ###

This task was about checking whether the discriminator can quickly learn to distinguish noise images from MNIST images. This was the case. We confirmed this with once feeding it directly the noise from the latent space, and with once feeding it noise from the untrained generator (experiment [ec8c72465](https://www.comet.ml/irratzo/exercise9/ec8c72465ca447289976399cc35a6ff9), tag `task1.I`). In both cases, the test accuracy after one epoch of training with a subset of the MNIST data was 1.   

### Code ###

(Note: in this earlier version, we used the term `fake` in lieu of equivalent `dream`.)

``` python
printcomet("""
# ==============================================================================
# Pretrain and -test the discriminator
# ==============================================================================
""")
# --------------------------------------------------
# - Create a dataset of 10000 real train images and 10000 fake images.
# - Train the discriminator for 1 epoch on this dataset.
# - Create a dataset of 5000 real test images and 5000 fake images.
# - Evaluate the test accuracy of your network.
# --------------------------------------------------


def sample_images(n, X, use_generator=True):
    """sample n//2 real (from X) and n//2 fake (latent space) data points
    
    if generate=False, sample fakes from uniform distribution instead of generator
    """
    half = n//2
    shape = list(X.shape)
    indices = np.random.randint(low=0, high=shape[0], size=half)
    shape[0] = half
    real = X[indices]
    if use_generator:
        gen_input_shape = (half, latent_dim)
        noise = np.random.uniform(low=0, high=1, size=gen_input_shape)
        fake = generator.predict(noise)
    else:
        fake = np.random.uniform(low=0, high=1, size=shape)
    labels = np.zeros((half*2,2))
    labels[:half, 0] = 1
    labels[half:, 1] = 1
    # samples = standardize(np.concatenate((real,fake)))
    samples = np.concatenate((real, fake))
    return samples, labels
    


# 'with'-context logs metrics with the prefix 'train_'
with experiment.train():
    pretrain_size = int(X_train.shape[0]/10)
    X_pretrain, y_pretrain = sample_images(pretrain_size, X_train)
    
    discriminator.fit(X_pretrain, y_pretrain,
              batch_size=32,
              epochs=1,
              verbose=2,
              shuffle=True
    )

    # history = model.fit() metrics automatically logged by comet: 
    # "loss", "accuracy", "val_loss", "val_accuracy" now become
    # "train_loss", "train_accuracy", "train_val_loss", "train_val_accuracy"
    

    
# 'with'-context logs metrics with the prefix 'test_'
with experiment.test():
    pretest_size = int(X_test.shape[0]/10)
    X_pretest, y_pretest = sample_images(pretest_size, X_test)
    
    loss, accuracy = discriminator.evaluate(X_pretest, y_pretest)
    printcomet("test_loss {}, test_accuracy {}".format(loss, accuracy))
    metrics = {
      "loss" : loss, # becomes "test_loss"
      "accuracy" : accuracy # becomes "test_accuracy"
    }
    experiment.log_metrics(metrics)
```

### Output ###

Experiment [ec8c72465](https://www.comet.ml/irratzo/exercise9/ec8c72465ca447289976399cc35a6ff9), tag `task1.I`

- **test_accuracy	1 at last step 188**

Charts:

`train_batch_accuracy`

![train_batch_accuracy.jpeg](https://www.comet.ml/api/image/notes/download?imageId=NiGdrR0yt9CKyP70294b87JxK&objectId=008b607c5ed743cf8ae7a9606aabdb09)

`train_batch_loss`

![train_batch_loss.jpeg](https://www.comet.ml/api/image/notes/download?imageId=OJOnk4uyoLIvNJbInsKhyvYEK&objectId=008b607c5ed743cf8ae7a9606aabdb09)

`test_accuracy,test_loss,train_accuracy,train_loss`

![test_accuracy,test_loss,train_accuracy,train_loss](https://www.comet.ml/api/image/notes/download?imageId=aWJ4CAwQ7J5e0fgnWElfjAqt9&objectId=008b607c5ed743cf8ae7a9606aabdb09)

## task1.II. Set up the main loop for training the adversarial framework (see slide 29) [10 P] ##

### Discussion ###

> Save the generator and discriminator loss for every training step

> Use uniform distributed noise as prior distribution

For correctly freezing the discriminator weights, we refer to our discussion of it in the subsection **GAN** above.

For sampling from the uniform distribution, we used `np.random.uniform(low=0, high=1)` throughout.

For training the GAN, we implemented a manual double loop over epochs and batches. The actual training is surprisingly short.

``` python
# train discriminator on half real, half dreamed images, labeled correctly
X, y = sample_images(batch_size, data, dream=True)
d_loss, d_acc = discriminator.train_on_batch(X, y)
# train gan on dreamed images with real labels to fool discriminator
# (note: gan receives latent space input, gen inside dreams up images)
X = sample_latent_space(batch_size)
y = create_labels(batch_size, real=True)
g_loss = gan.train_on_batch(X, y)
```

Another crucial point of the adversarial training process here is that we try to
fool the discriminator inside the `gan` here, by labeling the latent space input
as 'real' images. Since the discriminator is pretrained, in the beginning it
will not be fooled by the poor attempts of the generator and correctly label
these images as dreamed up. The optimizer will receive this as a large error and
subsequently adjust the weights strongly. But since all weights in the `gan` are
frozen except those of the generator, the generator learns as strongly as the
discriminator able to clearly discriminate.

### Code ###

We present results for task1.II and task1.III together below. As such, the code is the same. In this section, we try to only show the code relevant for subtask task1.II. The `main` part of the program (which calls the methods) is also found under task1.III.

Training loop:

``` python
# task1.II and task1.III
printcomet("""
# ==============================================================================
# Train GAN
# ==============================================================================
""")    

def train_gan(data, epoch_range, batch_size=32):
    with experiment.train() as exp:
        N = data.shape[0]
        batches_per_epoch = int(N / batch_size)
        half_batch = int(batch_size / 2)
        printcomet("training in epoch range {} with {} batches per epoch of size {}..."
              .format(list(epoch_range), batches_per_epoch, batch_size))
        for epoch in epoch_range:
            printcomet("epoch {}...".format(epoch))
            for batch in range(batches_per_epoch):
                # train discriminator on half real, half dreamed images, labeled correctly
                X, y = sample_images(batch_size, data, dream=True)
                d_loss, d_acc = discriminator.train_on_batch(X, y)
                # train gan on dreamed images with real labels to fool discriminator
                # (note: gan receives latent space input, gen inside dreams up images)
                X = sample_latent_space(batch_size)
                y = create_labels(batch_size, real=True)
                g_loss = gan.train_on_batch(X, y)
                metrics = {
                  "loss_discriminator" : d_loss, 
                  "acc_discriminator" : d_acc,
                  "loss_generator" : g_loss, 
                }
                experiment.log_metrics(metrics)
            monitor_training(data, epoch=epoch, rate=1)
```

Auxiliary sampling methods:

``` python
# sampling functions for training
def sample_latent_space(n, data_shape=None):
    """no data_shape: return shape (n, latent_dim), else shape data_shape with shape[0]=n.
    """
    if data_shape:
        shape = list(data_shape)
        shape[0] = n
    else:
        shape = (n, latent_dim)
    return np.random.uniform(low=0, high=1, size=shape)
        
def sample_dream_space(n):
    noise = np.random.uniform(low=0, high=1, size=(n, latent_dim))
    return generator.predict(noise)
        
def sample_real_space(n, data):
    indices = np.random.randint(low=0, high=data.shape[0], size=n)
    return data[indices]
    
def create_labels(n, real=True):
    y = np.zeros((n,2))
    if real:
        y[:, 0] = 1 # [1, 0] = 100% real
    else:
        y[:, 1] = 1 # [0, 1] = 100% dreamed
    return y
    
def sample_images(n, data, dream=True):
    """sample n//2 real (from data) and n//2 dreamed (generated) data points
    """
    half = n//2
    real = sample_real_space(half, data)
    dreamed = sample_dream_space(half)
    labels = np.concatenate((create_labels(half,True), create_labels(half,False)))
    # samples = standardize(np.concatenate((real,dreamed)))
    samples = np.concatenate((real, dreamed))
    return samples, labels
```

## task1.III. Generate some images after each training epoch. Plot training losses and generated images [5 P] ##

### Introduction ###

> Hand in the generator and discriminator loss and hand in the plots of
> generated samples

As noted under section task1.II, this section discusses the results for both task1.II and task1.III.

Judging from the references for this report, to date there seems no sure-fire way to automatically monitor evaluate GAN training. There are some indicators one can monitor during training whether one should stop and start again. For the MNIST dataset, these guidelines were found:

- The discriminator loss should hover around 0.5 to 0.8 per batch on the MNIST dataset. A crash on this loss indicates that the generator has started generating garbage and the discriminator has 'won' the game since its task is now very easy [2], and no more learning occurs [2]. 
- The generator / GAN loss should hover around 0.5 to 2 or higher on the MNIST dataset [2]. It is less critical.

For training, we logged the following metrics:

- `train_loss_discriminator`
- `train_acc_discriminator`
- `train_loss_generator` (from model `gan`)

If a run crashes, one could automatically restart. We did not implement this.

For evaluation, we did two things: in regular epoch intervals (in our case, *every* epoch), we performed a separate test of how well the discriminator can currently discriminate. We logged these accuracy metrics for real and dreamed images separately as 

- `test_acc_discriminator_epoch{}_real`
- `test_acc_discriminator_epoch{}_dream`

In addition, in the same epoch intervals we logged some generated images to see how well the generator currently dreams.

### Code ###

The `monitor` method used in the main training loop (see section task1.II above):

``` python
def monitor_training(data, epoch=0, rate=1, sample_size=100):
    assert (sample_size >= 16), "check_progress: sample_size must be >=16."
    if (epoch+1)%rate == 0:
        printcomet("epoch {}: monitor training progress...".format(epoch))
            # 'with'-context logs metrics with the prefix 'test_'
        with experiment.test():
            # discriminate real and dreamed images
            X = sample_real_space(sample_size, data)
            y = create_labels(sample_size, True)
            _, acc_real = discriminator.evaluate(X, y)
            X = sample_dream_space(sample_size)
            y = create_labels(sample_size, False)
            _, acc_dream = discriminator.evaluate(X, y)
            metrics = {
              "acc_discriminator_epoch{}_real".format(epoch) : acc_real, 
              "acc_discriminator_epoch{}_dream".format(epoch) : acc_dream
            }
            experiment.log_metrics(metrics)
            
            #plot some dream images
            filename = "dream_images_epoch{}.png".format(epoch)
            print("logging image {}...".format(filename))
            idx = np.random.choice(len(X), 16)
            fig = plot_images(X[idx], fname=filename)
            log_image(filename, path_data)  # always log your figures
```

The `main` code for the different experiments with varying parameters, discussed below:

``` python
def decrease_learning_rate(factor=5e-1):
    printcomet("decrease learning rate by factor {}".format(factor))
    g_opt.lr = g_opt.lr * factor
    d_opt.lr = d_opt.lr * factor

printcomet("""
# ------------------------------------------------------------------------------
# Experiment: train for 10 epochs with gradually decreasing learning rate
# ------------------------------------------------------------------------------
""")                  
batch_size = 32 # run3: 32, run1: 128
factor = 0.5 # run3: 0.5, run1: 0.1
printcomet("train for 4 epochs with original learning rates...")
train_gan(X_train, epoch_range=range(0,4), batch_size=batch_size)
decrease_learning_rate(factor)
printcomet("train for 3 epochs with reduced learning rates generator {}"
          ", discriminator {}:".format(g_opt.lr, d_opt.lr))
train_gan(X_train, epoch_range=range(4,7), batch_size=batch_size)
decrease_learning_rate(factor)
printcomet("train for 3 epochs with reduced learning rates generator {}"
          ", discriminator {}:".format(g_opt.lr, d_opt.lr))
train_gan(X_train, epoch_range=range(7,10), batch_size=batch_size)
printcomet("Done.")


# printcomet("""
# # ------------------------------------------------------------------------------
# # Experiment: train for 10 epochs with constant learning rate
# # ------------------------------------------------------------------------------
# """)   
# batch_size = 128
# train_gan(X_train, epoch_range=range(0,10), batch_size=batch_size)
```

### Results & Discussion ###

We performed four experiments. We ran all on VISPA with `pygpu --test %file`, so they stopped after 10 minutes even if not finished. For the purpose here, this sufficed.

- Experiment 0 [68a77508e](https://www.comet.ml/irratzo/exercise9/68a77508e9f642c29da0f98afc3fdfa4), tags `task1.III` and `run0_default`: 8 epochs and `batch_size` 128, with decreasing learning rate in three steps by a factor of 10 as suggested in provided `Exercise9.py`

``` text
train_loss_discriminator	0.019085358828306198	
train_loss_generator	6.410882949829102
```

- Experiment 1 [113b7dda1](https://www.comet.ml/irratzo/exercise9/113b7dda1ede4056b1a28a7616e44c35), tags `task1.III` and `run1_default`: same as above

``` text
train_loss_discriminator	0.07176630944013596	
train_loss_generator	5.906900405883789
```

- Experiment 2 [09b573746](https://www.comet.ml/irratzo/exercise9/09b57374657e45bd8e4408718d83160b), tags `task1.III` and `run2_constant-lr`: 10 epochs and `batch_size` 128, constant learning rate

``` text
train_loss_discriminator	0.25716179609298706	
train_loss_generator	5.700203895568848
```

- Experiment 3 [3ac997cd5](https://www.comet.ml/irratzo/exercise9/3ac997cd581340e9837e19e02a53e843), tags `task1.III` and `run3_bs32`: 10 epochs and `batch_size` 32, with decreasing learning rate in three steps by a factor of 2.

``` text
train_loss_discriminator	0.3622887432575226	
train_loss_generator	1.897776484489441	
```

The above noted 'safe' adversarial trade-off ranges of 0.5 to 0.8 for the discriminator loss and 0.5 to 2 or higher for the generator loss is only approached by the last experiment, Experiment 3, with the smaller batch size and the lower decrease in learning rate. In the experiments above, the discriminator's game is too easy, the generator too weak, so the former wins, at least in this short training time. A longer run should have been performed to see if this changes over time.

We close with showing the charts and plots of the experiments.

#### Plots for Experiment 0 ####
- Experiment 0 [68a77508e](https://www.comet.ml/irratzo/exercise9/68a77508e9f642c29da0f98afc3fdfa4), tags `task1.III` and `run0_default`: 8 epochs and `batch_size` 128, with decreasing learning rate in three steps by a factor of 10 as suggested in provided `Exercise9.py`

Charts.

`train_loss_discriminator`
![](https://www.comet.ml/api/image/notes/download?imageId=CGWFsAWgKHoQOhYbKaG8XsGWe&objectId=008b607c5ed743cf8ae7a9606aabdb09)

`train_loss_generator`
![](https://www.comet.ml/api/image/notes/download?imageId=eYOqm4cw7IyMACSIsKMNjVdpP&objectId=008b607c5ed743cf8ae7a9606aabdb09)

`train_accuracy_discriminator`
![](https://www.comet.ml/api/image/notes/download?imageId=EySfxfIPIkK1B1KYkC0UNVAoM&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 0.
![](https://www.comet.ml/api/image/notes/download?imageId=rTHaqbkPCiOPnk1xaMvrQBnSK&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 1.
![](https://www.comet.ml/api/image/notes/download?imageId=bexRIZlxbzNs1SHQ5VOKB1iok&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 2.
![](https://www.comet.ml/api/image/notes/download?imageId=FL1eUiITr9i5O8KYnFTueDBl8&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 3.
![](https://www.comet.ml/api/image/notes/download?imageId=T1NuIqiyzbZ8bL6AvdhVfOEbd&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 4.
![](https://www.comet.ml/api/image/notes/download?imageId=5TF7vJdOuOxYO14r5jLY0xsaW&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 5.
![](https://www.comet.ml/api/image/notes/download?imageId=UfVe7gVYi3Vk6Bp5ivw447GgA&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 6.
![](https://www.comet.ml/api/image/notes/download?imageId=DCmfrx0XlSJSx1TTotUi41Y70&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 7.
![](https://www.comet.ml/api/image/notes/download?imageId=TYxiCTEniqI5BHggzhc2LSNur&objectId=008b607c5ed743cf8ae7a9606aabdb09)

#### Plots for Experiment 1 ####
- Experiment 1 [113b7dda1](https://www.comet.ml/irratzo/exercise9/113b7dda1ede4056b1a28a7616e44c35), tags `task1.III` and `run1_default`: same as above

Charts.

`train_loss_discriminator`
![](https://www.comet.ml/api/image/notes/download?imageId=gwvfc4pC6lQuGHW8o3UkMkHtt&objectId=008b607c5ed743cf8ae7a9606aabdb09)

`train_loss_generator`
![](https://www.comet.ml/api/image/notes/download?imageId=LOAc0D3gw819wNX03eKh8donB&objectId=008b607c5ed743cf8ae7a9606aabdb09)

`train_accuracy_discriminator`
![](https://www.comet.ml/api/image/notes/download?imageId=Kb4yJ4OPHeEBvwYqHTC7166oa&objectId=008b607c5ed743cf8ae7a9606aabdb09)

`test_acc_discriminator_dream/real_epochX`
![](https://www.comet.ml/api/image/notes/download?imageId=8xXInaq7LXZy4cmz6QRRpX1pR&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 0.
![](https://www.comet.ml/api/image/notes/download?imageId=LDsnL2gS87GiykFVqebmGSc79&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 1.
![](https://www.comet.ml/api/image/notes/download?imageId=G2sEaPPUMgpEDab7kn4r3F9GT&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 2.
![](https://www.comet.ml/api/image/notes/download?imageId=ZFFe0QHW1ttEz7JRRO9WUTyeb&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 3.
![](https://www.comet.ml/api/image/notes/download?imageId=KXKtjOonPuSH8cVAFg3J4neXu&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 4.
![](https://www.comet.ml/api/image/notes/download?imageId=hpMDgPRMBd2DhRGy09J0Y0wbc&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 5.
![](https://www.comet.ml/api/image/notes/download?imageId=h34dBDhfYEl3dk5ZVVhfsoT8g&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 6.
![](https://www.comet.ml/api/image/notes/download?imageId=sKhiwJF5HmCNFpeVCmWu5YDXg&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 7.
![](https://www.comet.ml/api/image/notes/download?imageId=LlAYTp3xs7sNkNu8iLutLFBrD&objectId=008b607c5ed743cf8ae7a9606aabdb09)

#### Plots for Experiment 2 ####
- Experiment 2 [09b573746](https://www.comet.ml/irratzo/exercise9/09b57374657e45bd8e4408718d83160b), tags `task1.III` and `run2_constant-lr`: 10 epochs and `batch_size` 128, constant learning rate

Charts.

`train_loss_discriminator`
![](https://www.comet.ml/api/image/notes/download?imageId=846dtjY64Qo3trx2uYrOqr8xN&objectId=008b607c5ed743cf8ae7a9606aabdb09)

`train_loss_generator`
![](https://www.comet.ml/api/image/notes/download?imageId=1CaoKdj8z8adUkVwsVeqao0xa&objectId=008b607c5ed743cf8ae7a9606aabdb09)

`train_accuracy_discriminator`
![](https://www.comet.ml/api/image/notes/download?imageId=Vbrp2NytCmUUAsQXz6FKs6yCR&objectId=008b607c5ed743cf8ae7a9606aabdb09)

`test_acc_discriminator_dream/real_epochX`
![](https://www.comet.ml/api/image/notes/download?imageId=1JaR3JhU6Rg2h6MF1I7ujHUmG&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 0.
![](https://www.comet.ml/api/image/notes/download?imageId=FjZSI52qNfuJtXc6LHdkBtfsO&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 1.
![](https://www.comet.ml/api/image/notes/download?imageId=UiAolaZKpjuMet1o8Luu5TueV&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 2.
![](https://www.comet.ml/api/image/notes/download?imageId=S3toYz6PzSBAH98S8bQ5Xv0UK&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 3.
![](https://www.comet.ml/api/image/notes/download?imageId=Vj3molsVjJ5nU6fGLPEtQyjvj&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 4.
![](https://www.comet.ml/api/image/notes/download?imageId=R76FXurYe8TY01p03yNyvksyo&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 5.
![](https://www.comet.ml/api/image/notes/download?imageId=mzYdG7rchhMldfbWdtx1r9BBB&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 6.
![](https://www.comet.ml/api/image/notes/download?imageId=w0swDjtZlBhEffYcWvGyHAz3c&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 7.
![](https://www.comet.ml/api/image/notes/download?imageId=TbrNjpk57fN6ABshMepa3eZwK&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 8.
![](https://www.comet.ml/api/image/notes/download?imageId=GSeRUmvTz3V2YhenWi08KsiMQ&objectId=008b607c5ed743cf8ae7a9606aabdb09)

#### Plots for Experiment 3 ####
- Experiment 3 [3ac997cd5](https://www.comet.ml/irratzo/exercise9/3ac997cd581340e9837e19e02a53e843), tags `task1.III` and `run3_bs32`: 10 epochs and `batch_size` 32, with decreasing learning rate in three steps by a factor of 2

Charts.

`train_loss_discriminator`
![](https://www.comet.ml/api/image/notes/download?imageId=TWcOEAJ8PNBzD3gt5sL2coaE6&objectId=008b607c5ed743cf8ae7a9606aabdb09)

`train_loss_generator`
![](https://www.comet.ml/api/image/notes/download?imageId=S1aPBrP9nfC5tEst6UtwCoWxZ&objectId=008b607c5ed743cf8ae7a9606aabdb09)

`train_accuracy_discriminator`
![](https://www.comet.ml/api/image/notes/download?imageId=L2tQLZUvJPI4XPgN6zptHyAMq&objectId=008b607c5ed743cf8ae7a9606aabdb09)

`test_acc_discriminator_dream-real_epochX`
![](https://www.comet.ml/api/image/notes/download?imageId=97oBuSCDYztpUfzi9JkggZkuS&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 0.
![](https://www.comet.ml/api/image/notes/download?imageId=VwXy20hO43wK2rzwoi43XMEoa&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 1.
![](https://www.comet.ml/api/image/notes/download?imageId=H2E2mmryc5GnHIrhr1T3uBnPD&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 2.
![](https://www.comet.ml/api/image/notes/download?imageId=bmquxUOd4tEbbHIyWbVgWDYo2&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 3.
![](https://www.comet.ml/api/image/notes/download?imageId=mx2vEdqVInSPCFPjxh69XICgy&objectId=008b607c5ed743cf8ae7a9606aabdb09)

Dream images, epoch 4.
![](https://www.comet.ml/api/image/notes/download?imageId=DWS8zdf9ncj6ODrzmURYqkF9S&objectId=008b607c5ed743cf8ae7a9606aabdb09)

# References #

- [1] RWTH DLiPR SS20 Part 9: Generative Methods. Lecture Slides. 2020.
- [2] machinelearningmastery.com. How to Develop a GAN for Generating MNIST Handwritten Digits. July 12, 2019. URL: https://machinelearningmastery.com/how-to-develop-a-generative-adversarial-network-for-an-mnist-handwritten-digits-from-scratch-in-keras/
- [3] Tensorflow Tutorials. Deep Convolutional Generative Adversarial Network. Retrieved June 28, 2020. URL: https://www.tensorflow.org/tutorials/generative/dcgan 
- [4] MIT 6.S191 Introduction to Deep Learning. Lecture 4 Deep Generative Modeling. January 28, 2020. URL: http://introtodeeplearning.com/
- [5] CS231n Convolutional Neural Networks for Visual Recognition. Spring 2020. Course Website. Module 1: Neural Networks. Neural Networks Part 2: Setting up the Data and the Loss. URL: https://cs231n.github.io/neural-networks-2/
- [6] Wang et. al. CNN Explainer: Learning Convolutional Neural Networks with Interactive Visualization. April 30, 2020. URL with browser live demo: https://github.com/poloclub/cnn-explainer 
- [7] Kahng et al. GAN Lab: Understanding Complex Deep Generative Models using Interactive Visual Experimentation. January 2019. URL with live demo, just like playground.tensorflow.org, but for GANs: https://github.com/poloclub/ganlab

