from comet_ml import Experiment
import numpy as np
from tensorflow import keras
import dlipr
from gan import generator_model, discriminator_model, plot_images, make_trainable
layers = keras.layers
models = keras.models
opt = keras.optimizers

# Set up YOUR experiment - login to comet, create new project (for new exercise)
# and copy the statet command
# or just change the name of the workspace, and the API (you can find it in the settings)
experiment = Experiment(api_key="EnterYourAPIKey",
                        project_name="exercise9", workspace="EnterGroupWorkspaceHere")


# prepare MNIST dataset
data = dlipr.mnist.load_data()
X_train = data.train_images.reshape(-1, 28, 28, 1) / 255.
X_test = data.test_images.reshape(-1, 28, 28, 1) / 255.

# plot some real images
idx = np.random.choice(len(X_train), 16)
fig = plot_images(X_train[idx], fname='real_images.png')
experiment.log_figure(figure=fig)  # always log your figures


# --------------------------------------------------
# Set up generator, discriminator and GAN (stacked generator + discriminator)
# Feel free to modify the provided models.
# --------------------------------------------------
# Set up generator
# we don't call the generator by itself, hence no compilation needed
print('\nGenerator')
latent_dim = 100
g_input = layers.Input(shape=[latent_dim])
g_output = generator_model(g_input)
generator = models.Model(g_input, g_output)
print(generator.summary())

# Set up discriminator
print('\nDiscriminator')
d_input = layers.Input(shape=(28, 28, 1))
d_output = discriminator_model(d_input)
discriminator = models.Model(d_input, d_output)
# !!! Freeze / Unfreeze the model here
print(discriminator.summary())
d_opt = opt.Adam(lr=1e-3)
discriminator.compile(loss='binary_crossentropy', optimizer=d_opt, metrics=['accuracy'])

# Set up GAN by stacking the discriminator on top of the generator
print('\nGenerative Adversarial Network')
gan_input = layers.Input(shape=[latent_dim])
gan_output = discriminator(generator(gan_input))
GAN = models.Model(gan_input, gan_output)
print(GAN.summary())
g_opt = opt.Adam(lr=1e-4)
# !!! Freeze / Unfreeze the model here
GAN.compile(loss='binary_crossentropy', optimizer=g_opt)


# --------------------------------------------------
# Pretrain the discriminator:
# - Create a dataset of 10000 real train images and 10000 fake images.
# - Train the discriminator for 1 epoch on this dataset.
# - Create a dataset of 5000 real test images and 5000 fake images.
# - Evaluate the test accuracy of your network.
# --------------------------------------------------


# --------------------------------------------------
# Implement main training loop:
# - Repeat for number of epochs
#   - Repeat for one epoch
#      - Create a mini-batch of data (X: real images + fake images, y: corresponding class vectors)
#      - Train the discriminator on the mini-batch
#      - Freeze the discriminator: make_trainable(discriminator, False)
#        You have to compile the model afterwards,  hence do it during model definition
#      - Create a mini-batch of data (X: noise, y: class vectors pretending that these produce real images)
#      - Train the generator on the mini-batch
#      - Unfreeze the discriminator: make_trainable(discriminator, True)
#        You have to compile the model afterwards, hence do it during model definition
#   - Plot some fake images
# - Plot the loss of discriminator and generator as function of training steps
#
# Note: Training GANs requires careful tuning of the learning rates.
# You can try following
# - Train for 3 epochs with original learning rates
# - Train for 3 epochs at reduced learning rates
#     g_opt.lr = 1e-5
#     d_opt.lr = 1e-4
# - Train for 2 epochs at reduced learning rates
#     g_opt.lr = 1e-6
#     d_opt.lr = 1e-5
# --------------------------------------------------
