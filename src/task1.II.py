# ==============================================================================
# exercise9.1: GAN for MNIST
# ==============================================================================

# Usage:
# none

# ==============================================================================
# Constants, Imports
# ==============================================================================
# constants
COMET_EXPERIMENT = True
NOTEBOOK = False
REFERENCES = [
        "https://git.rwth-aachen.de/3pia/vispa/dlipr", # [1]
        "https://github.com/DavidWalz/dlipr", # [2]
    ]
    
CLASS_LABELS = ["real", "dreamed"]


# setup comet
# for notebook (eg google colab). only needed if comet_ml not installed yet.
if COMET_EXPERIMENT and NOTEBOOK:
    pass # if notebook, uncomment:
    # %pip install comet_ml
# after installation for first time, restart notebook!

if COMET_EXPERIMENT:
    # import comet_ml in the top of your file    
    from comet_ml import Experiment
    
    # Add the following code anywhere in your machine learning file
    experiment = Experiment(api_key="EnterYourAPIKey",
                            project_name="exercise9", workspace="irratzo")
       
# imports from provided Exercise9.py
import numpy as np
from tensorflow import keras
import dlipr
from gan import generator_model, discriminator_model, plot_images, make_trainable
layers = keras.layers
models = keras.models
opt = keras.optimizers

# other imports  
import matplotlib.pyplot as plt
import tensorflow as tf
from sklearn.preprocessing import StandardScaler
import os
from pathlib import Path
models = keras.models
layers = keras.layers                            

# some utility logging methods    
def printcomet(string : str = ""):
    # append references (like for "... [1] ..." and so on)
    reflabl = ["[{}]".format(i+1) for i in range(len(REFERENCES))]
    if any([labl in string for labl in reflabl]):
      string += "\nReferences:"
      for i,labl in enumerate(reflabl):
        if labl in string:
          string += "\n{} {}".format(labl, REFERENCES[i])
    
    # print and log
    print(string)
    if COMET_EXPERIMENT:
        experiment.log_text(string)
        
def log_image(filename : str = "unnamed", path : Path = None):
    # the nice thing about vispa is that if the image stays
    # in the src folder, it will be displayed upon execution.
    # the bad thing about that is that it dirties the src folder.
    # alternative: put into a data folder.
    if path:
        path.mkdir(parents=True, exist_ok=True)
        Path(filename).replace(path / filename)
    else:
        path = Path.cwd() # = don't move file
    if COMET_EXPERIMENT:
        experiment.log_image(str(path / filename))
        
def plot_and_log_image(filename : str = "unnamed", path : Path = None, array=None, 
                       figsize=(10,10), cmap=plt.cm.Greys, axis='off'):
    fig, ax = plt.subplots(figsize=figsize)
    ax.imshow(array, cmap=cmap)
    ax.axis('off')
    plt.tight_layout()
    fig.savefig(filename)
    log_image(filename, path)     
    
def stats(array):
    printcomet("shape {}, type {}".format(array.shape, array.dtype))
    printcomet("(min,max)=({:.2e},{:.2e}), mean={:.2e}, std={:.2e}".format(
        np.amin(array), np.amax(array), np.mean(array), np.std(array)))
    
# preprocess the data in a suitable way
def standardize(*arrays):
    """first array used for mean, std. creates copies.
    
    Examples
    ========
    
    >>> x_train = standardize(x_train)
    >>> x_train, x_test = standardize(x_train, x_test)
    >>> x_train, x_test, x_foo = standardize(x_train, x_test, x_foo)
    """
    first = arrays[0]
    rest = arrays[1:]
    astype = 'float32'

    shape = first.shape
    M = shape[0] # no. of data points
    N = first.size//M # size of one data point
    firs = first.flatten().reshape(M, N) # scaler needs 1D data
    scaler = StandardScaler()
    scaler.fit(firs)
    firs = scaler.transform(firs).reshape(*shape).astype(astype) # CNN needs 2+D data
    
    arrs = [firs,]
    for arr in rest:
        shape = arr.shape
        M = shape[0]
        N = arr.size//M
        arrs.append(scaler.transform(
            arr.flatten().reshape(M,N)).reshape(*shape).astype(astype))

    # # alternative equivalent method, less precise:
    #
    # mean = np.mean(first, axis=0)
    # stdev = np.std(first, axis=0)
    # firs = np.zeros_like(first).astype(astype)
    # for i in range(first.shape[0]):
    #   firs[i] = (first[i] - mean) / stdev
    # arrs = [firs,]
    # for arr in rest:
    #     ar = np.zeros_like(arr).astype(astype)
    #     for i in range(arr.shape[0]):
    #       ar[i] = (arr[i] - mean) / stdev
    #     arrs.append(ar)
    
    if len(arrs)==1:
        return arrs[0]
    else:
        return tuple(arrs)

# make output directory ../data/results_taskX.Y_runZ
output_label = "task1.II_run1"
path_src = Path.cwd()
path_data = path_src.parent / "data" / output_label
path_models = path_src.parent / "models" / output_label
printcomet("local output dirs:\n- data: {}\n- models: {}\n- src: {}".format(path_data, path_models, path_src))

printcomet("Constants: COMET_EXPERIMENT {}, NOTEBOOK {}".format(
    COMET_EXPERIMENT, NOTEBOOK))

printcomet("""
# ==============================================================================
# Data Preprocessing
# ==============================================================================
""")

# load dataset
data = dlipr.mnist.load_data()
X_train = np.expand_dims(data.train_images, axis=-1)
X_test = np.expand_dims(data.test_images, axis=-1)
printcomet("X_train stats before preprocessing:")
stats(X_train)

# # # different preprocessing methods (choose ONE) (we choose 1, see report)):
# 1) provided Exercise9.py preprocessing
X_train = X_train.reshape(-1, 28, 28, 1) / 255.
X_test = X_test.reshape(-1, 28, 28, 1) / 255.
# # 2) our own preprocessing
# X_train, X_test = standardize(X_train, X_test)
# # 3) tensorflow GAN MNIST tutorial preprocessing
# X_train = X_train.reshape(X_train.shape[0], 28, 28, 1).astype('float32')
# X_train = (X_train - 127.5) / 127.5 # Normalize the images to [-1, 1]

# see what preprocessing achieved
stats(X_train)

# plot some real images
idx = np.random.choice(len(X_train), 16)
filename = 'real_images.png'
fig = plot_images(X_train[idx], fname=filename)
log_image(filename, path_data)  # always log your figures


# --------------------------------------------------
# Set up generator, discriminator and GAN (stacked generator + discriminator)
# Feel free to modify the provided models.
# --------------------------------------------------
printcomet("""
# ==============================================================================
# Set up generator
# ==============================================================================
""")
# we don't call the generator by itself, hence no compilation needed
latent_dim = 100
g_input = layers.Input(shape=[latent_dim])
g_output = generator_model(g_input)
generator = models.Model(g_input, g_output, name="Generator")
generator.summary(print_fn=printcomet)

# confirm that an untrained generator just spits out noise
noise = tf.random.uniform([1, 100])
generated_image = generator(noise, training=False)
plot_and_log_image('generator_untrained_output.png', path_data, 
                  generated_image[0, :, :, 0], figsize=(2.5,2.5))

printcomet("""
# ==============================================================================
# Set up discriminator
# ==============================================================================
""")
d_input = layers.Input(shape=(28, 28, 1))
d_output = discriminator_model(d_input)
discriminator = models.Model(d_input, d_output, name="Discriminator")
# !!! Freeze / Unfreeze the model here
# for discriminator compilation, need discriminator trainability!
make_trainable(discriminator, True)
discriminator.summary(print_fn=printcomet)
d_opt = opt.Adam(lr=1e-3)
discriminator.compile(loss='binary_crossentropy', optimizer=d_opt, metrics=['accuracy'])

# confirm that the untrained discriminator can't discriminate
decision = discriminator(generated_image).numpy()
printcomet("decision from untrained discriminator on image from untrained generator"\
           "(probabilities of image being dreamed or real: {}".format(decision))

printcomet("""
# ==============================================================================
# Set up GAN
# ==============================================================================
""")
# stack generator and discriminator on top of each other
gan_input = layers.Input(shape=[latent_dim]) # == g_input
gan_output = discriminator(generator(gan_input)) 
gan = models.Model(gan_input, gan_output, name="GAN")
gan.summary(print_fn=printcomet)
g_opt = opt.Adam(lr=1e-4)
# !!! Freeze / Unfreeze the model here
# for GAN compilation, need discriminator untrainability! 
make_trainable(discriminator, False)
gan.compile(loss='binary_crossentropy', optimizer=g_opt)


# sampling functions for training
def sample_latent_space(n, data_shape=None):
    """no data_shape: return shape (n, latent_dim), else shape data_shape with shape[0]=n.
    """
    if data_shape:
        shape = list(data_shape)
        shape[0] = n
    else:
        shape = (n, latent_dim)
    return np.random.uniform(low=0, high=1, size=shape)
        
def sample_dream_space(n):
    noise = np.random.uniform(low=0, high=1, size=(n, latent_dim))
    return generator.predict(noise)
        
def sample_real_space(n, data):
    indices = np.random.randint(low=0, high=data.shape[0], size=n)
    return data[indices]
    
def create_labels(n, real=True):
    y = np.zeros((n,2))
    if real:
        y[:, 0] = 1 # [1, 0] = 100% real
    else:
        y[:, 1] = 1 # [0, 1] = 100% dreamed
    return y
    
def sample_images(n, data, dream=True):
    """sample n//2 real (from data) and n//2 dreamed (latent space) data points
    
    if dream=False, sample dreams from uniform distribution instead of generator
    """
    half = n//2
    real = sample_real_space(half, data)
    dreamed = sample_dream_space(half)
    labels = np.concatenate((create_labels(half,True), create_labels(half,False)))
    # samples = standardize(np.concatenate((real,dreamed)))
    samples = np.concatenate((real, dreamed))
    return samples, labels

pretrain = False # see task1.I.py
if pretrain:
    printcomet("""
    # ==============================================================================
    # Pretrain and -test the discriminator
    # ==============================================================================
    """)
    # --------------------------------------------------
    # - Create a dataset of 10000 real train images and 10000 dreamed images.
    # - Train the discriminator for 1 epoch on this dataset.
    # - Create a dataset of 5000 real test images and 5000 dreamed images.
    # - Evaluate the test accuracy of your network.
    # --------------------------------------------------

    # 'with'-context logs metrics with the prefix 'train_'
    with experiment.train():
        pretrain_size = int(X_train.shape[0]/10)
        X_pretrain, y_pretrain = sample_images(pretrain_size, X_train)
        
        discriminator.fit(X_pretrain, y_pretrain,
                  batch_size=32,
                  epochs=1,
                  verbose=2,
                  shuffle=True
        )
    
        # history = model.fit() metrics automatically logged by comet: 
        # "loss", "accuracy", "val_loss", "val_accuracy" now become
        # "train_loss", "train_accuracy", "train_val_loss", "train_val_accuracy"
        
    
    # 'with'-context logs metrics with the prefix 'test_'
    with experiment.test():
        pretest_size = int(X_test.shape[0]/10)
        X_pretest, y_pretest = sample_images(pretest_size, X_test)
        
        loss, accuracy = discriminator.evaluate(X_pretest, y_pretest)
        printcomet("test_loss {}, test_accuracy {}".format(loss, accuracy))
        metrics = {
          "loss" : loss, # becomes "test_loss"
          "accuracy" : accuracy # becomes "test_accuracy"
        }
        experiment.log_metrics(metrics)
    

# task1.II
printcomet("""
# ==============================================================================
# Train GAN
# ==============================================================================
""")    
# --------------------------------------------------
# Implement main training loop:
# - Repeat for number of epochs
#   - Repeat for one epoch
#      - Create a mini-batch of data (X: real images + dreamed images, y: corresponding class vectors)
#      - Train the discriminator on the mini-batch
#      - Freeze the discriminator: make_trainable(discriminator, False)
#        You have to compile the model afterwards,  hence do it during model definition
#      - Create a mini-batch of data (X: noise, y: class vectors pretending that these produce real images)
#      - Train the generator on the mini-batch
#      - Unfreeze the discriminator: make_trainable(discriminator, True)
#        You have to compile the model afterwards, hence do it during model definition
#   - Plot some dreamed images
# - Plot the loss of discriminator and generator as function of training steps
#
# Note: Training GANs requires careful tuning of the learning rates.
# You can try following
# - Train for 3 epochs with original learning rates
# - Train for 3 epochs at reduced learning rates
#     g_opt.lr = 1e-5
#     d_opt.lr = 1e-4
# - Train for 2 epochs at reduced learning rates
#     g_opt.lr = 1e-6
#     d_opt.lr = 1e-5
# --------------------------------------------------

def check_progress(experiment, data, run=0, epoch=0, rate=1, sample_size=100):
    if (epoch+1)%rate == 0:
        printcomet("epoch {}: check training progress...")
        # discriminate real and dreamed images
        X = sample_real_space(sample_size, data)
        y = create_labels(sample_size, True)
        _, acc_real = discriminator.evaluate(X, y)
        X = sample_dream_space(sample_size)
        y = create_labels(sampel_size, False)
        _, acc_dream = discriminator.evaluate(X, y)
        metrics = {
          "accuracy_discriminator_real" : acc_real, 
          "accuracy_discriminator_dream" : acc_dream
        }
        experiment.log_metrics(metrics)
        filename = "dream_run{}_epoch{}.png".format(run,epoch)
        print("logging image {}...".format(filename))
        idx = np.random.choice(len(X), 16)
        fig = plot_images(X[idx], fname=filename)
        log_image(filename, path_data)  # always log your figures

def train_gan(data, epoch_range, batch_size=32):
    with experiment.train() as exp:
        N = data.shape[0]
        batches_per_epoch = int(N / batch_size)
        half_batch = int(batch_size / 2)
        printcomet("training in epoch range {} with {} batches per epoch of size {}..."
              .format(list(epoch_range), batches_per_epoch, batch_size))
        for epoch in epoch_range:
            printcomet("epoch {}...".format(epoch))
            for batch in range(batches_per_epoch):
                # train discriminator on half real, half dreamed images, labeled correctly
                X, y = sample_images(batch_size, data, dream=True)
                d_loss, d_acc = discriminator.train_on_batch(X, y)
                # train gan on dreamed images with real labels to fool discriminator
                # (note: gan receives latent space input, gen inside dreams up images)
                X = sample_latent_space(batch_size)
                y = create_labels(batch_size, real=True)
                g_loss = gan.train_on_batch(X, y)
                metrics = {
                  "loss_discriminator" : d_loss, 
                  "accuracy_discriminator" : d_acc,
                  "loss_generator" : g_loss, 
                }
                experiment.log_metrics(metrics)
            check_progress(exp, data, run=0, epoch=epoch, rate=1)
            
            
                
batch_size = 128
printcomet("train for 3 epochs with original learning rates...")
train_gan(X_train, epoch_range=(0,3), batch_size=batch_size)
g_opt.lr = 1e-5
d_opt.lr = 1e-4
printcomet("train for 3 epochs with reduced learning rates generator {}"
           ", discriminator {}:".format(g_opt.lr, d_opt.lr)
train_gan(X_train, epoch_range=(3,6), batch_size=batch_size)
g_opt.lr = 1e-6
d_opt.lr = 1e-5
printcomet("train for 2 epochs with reduced learning rates generator {}"
           ", discriminator {}:".format(g_opt.lr, d_opt.lr)
train_gan(X_train, epoch_range=(6,8), batch_size=batch_size)
printcomet("Done.")
